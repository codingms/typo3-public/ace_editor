<?php

defined('TYPO3') or die();

// Registers a Backend Module
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
    'ace_editor',
    'file',
    'aceeditor',
    '',
    [
        \CodingMs\AceEditor\Controller\AceEditorController::class => 'list,edit,save',
    ],
    [
        'access' => 'user,group',
        'icon' => 'EXT:ace_editor/Resources/Public/Icons/module-aceeditor.svg',
        'iconIdentifier' => 'module-aceeditor',
        'labels' => 'LLL:EXT:ace_editor/Resources/Private/Language/locallang_aceeditor.xlf',
    ]
);

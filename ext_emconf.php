<?php
$EM_CONF['ace_editor'] = [
    'title' => 'ACE editor for file list',
    'description' => 'ACE editor for file list',
    'category' => 'plugin',
    'author' => 'Thomas Deuling',
    'author_email' => 'typo3@coding.ms',
    'author_company' => 'coding.ms',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '2.2.0',
    'constraints' => [
        'depends' => [
            'php' => '7.2.0-8.1',
            'typo3' => '10.4.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];

# Ace-Editor Extension for TYPO3

This extension provides an ACE-Editor for TYPO3 backend.


**Features:**

*   Convenient editing of source files within the filelist
*   Easy to understand Backend module
*   Extensive editing features such as syntax highlighting

If you need some additional or custom feature - get in contact!


**Links:**

*   [Ace-Editor Documentation](https://www.coding.ms/documentation/typo3-ace-editor "Ace-Editor Documentation")
*   [Ace-Editor Bug-Tracker](https://gitlab.com/codingms/typo3-public/ace_editor/-/issues "Ace-Editor Bug-Tracker")
*   [Ace-Editor Repository](https://gitlab.com/codingms/typo3-public/ace_editor "Ace-Editor Repository")
*   [Ace Editor Website](https://ace.c9.io/ "Ace Editor Website")
*   [TYPO3 Ace-Editor Produktdetails](https://www.coding.ms/typo3-extensions/typo3-ace-editor/ "TYPO3 Ace-Editor Produktdetails")
*   [TYPO3 Extension Ace Editor Download](https://extensions.typo3.org/extension/ace_editor/ "TYPO3 Extension Ace Editor Download")

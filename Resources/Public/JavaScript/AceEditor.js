/**
 * Ace editor
 */
define(['ace/ace', 'jquery'], function (ace, jQuery) {

    this.editor = ace.edit('editor');

    this.saveCommand = function (editor) {
        var content = editor.getSession().getValue();
        var uriSave = jQuery('#editor-form-save').attr('action');
        jQuery.post(uriSave, {
                'tx_aceeditor_file_aceeditoraceeditor[content]': content
            },
            function (response) {
                if (typeof (response.error) !== 'undefined') {
                    alert(response.error);
                    jQuery('.ace_content').css('background-color', '#660000');
                    setTimeout(function () {
                        jQuery('.ace_content').css('background-color', 'transparent')
                    }, 300);
                } else {
                    jQuery('.ace_content').css('background-color', '#006600');
                    setTimeout(function () {
                        jQuery('.ace_content').css('background-color', 'transparent')
                    }, 300);
                }
            }
        );
    };

    this.initialize = function () {
        var ace = this;
        this.editor.setTheme('ace/theme/monokai');
        var content = jQuery('#content');
        var type = content.attr('data-type');
        if (type === 'html' || type === 'htm' || type === 'tmpl') {
            this.editor.getSession().setMode('ace/mode/html');
        } else if (type === 'js') {
            this.editor.getSession().setMode('ace/mode/javascript');
        } else if (type === 'json') {
            this.editor.getSession().setMode('ace/mode/json');
        } else if (type === 'php') {
            this.editor.getSession().setMode('ace/mode/php');
        } else if (type === 'css') {
            this.editor.getSession().setMode('ace/mode/css');
        } else if (type === 'scss') {
            this.editor.getSession().setMode('ace/mode/scss');
        } else if (type === 'sass') {
            this.editor.getSession().setMode('ace/mode/sass');
        } else if (type === 'less') {
            this.editor.getSession().setMode('ace/mode/less');
        } else if (type === 'sql') {
            this.editor.getSession().setMode('ace/mode/mysql');
        } else if (type === 'csv') {
            this.editor.getSession().setMode('ace/mode/text');
        } else if (type === 'xml' || type === 'xlf') {
            this.editor.getSession().setMode('ace/mode/json');
        } else if (type === 'yaml' || type === 'yml') {
            this.editor.getSession().setMode('ace/mode/yaml');
        } else if (type === 'txt' || type === 'typoscript' || type === 'pagets' || type === 'setupts' || type === 'constantsts' || type === 'ts') {
            this.editor.getSession().setMode('ace/mode/javascript');
        } else {
            this.editor.getSession().setMode('ace/mode/' + type);
        }
        this.editor.getSession().setValue(content.val());
        this.editor.commands.addCommand({
            name: 'save',
            bindKey: {
                win: 'Ctrl-S',
                mac: 'Command-S',
                sender: 'editor|cli'
            },
            exec: this.saveCommand
        });

        jQuery('.btn.editor-save').click(function () {
            var content = ace.editor.getSession().getValue();
            var uriSave = jQuery('#editor-form-save').attr('action');
            jQuery.post(uriSave, {
                    'tx_aceeditor_file_aceeditoraceeditor[content]': content
                },
                function (response) {
                    if (typeof (response.error) !== 'undefined') {
                        alert(response.error);
                        jQuery('.ace_content').css('background-color', '#660000');
                        setTimeout(function () {
                            jQuery('.ace_content').css('background-color', 'transparent')
                        }, 300);
                    } else {
                        jQuery('.ace_content').css('background-color', '#006600');
                        setTimeout(function () {
                            jQuery('.ace_content').css('background-color', 'transparent')
                        }, 300);
                    }
                }
            );
            return false;
        });

        jQuery('.btn.editor-save-and-close').click(function () {
            var content = ace.editor.getSession().getValue();
            var uriSave = jQuery('#editor-form-save').attr('action');
            jQuery.post(uriSave, {
                    'tx_aceeditor_file_aceeditoraceeditor[content]': content
                },
                function (response) {
                    if (typeof (response.error) !== 'undefined') {
                        alert(response.error);
                        jQuery('.ace_content').css('background-color', '#660000');
                        setTimeout(function () {
                            jQuery('.ace_content').css('background-color', 'transparent')
                        }, 300);
                    } else {
                        jQuery('.ace_content').css('background-color', '#006600');
                        setTimeout(function () {
                            jQuery('.ace_content').css('background-color', 'transparent');
                            location.href = jQuery('.btn.editor-close').attr('href');
                        }, 300);
                    }
                }
            );
            return false;
        });

    };

    return this.initialize();

});

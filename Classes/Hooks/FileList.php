<?php

/**
 * Extends the file list.
 */

namespace CodingMs\AceEditor\Hooks;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Filelist\FileListEditIconHookInterface;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Backend\Routing\UriBuilder;

/**
 * Extends the file list.
 *
 * @hook   TYPO3_CONF_VARS|SC_OPTIONS|fileList|editIconsHook
 */
class FileList implements FileListEditIconHookInterface
{
    /**
     * Modifies edit icon array.
     *
     * @param array $cells Array of edit icons
     * @param \TYPO3\CMS\Filelist\FileList $parentObject Parent object
     */
    public function manipulateEditIcons(&$cells, &$parentObject)
    {
        $file = $this->getFile($cells);
        if ($file instanceof FileInterface) {
            $parameter = [
                'tx_aceeditor_file_aceeditoraceeditor' => [
                    'controller' => 'AceEditor',
                    'action' => 'edit',
                    'file' => $file->getUid(),
                ],
            ];

            $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
            $url = (string)$uriBuilder->buildUriFromRoute('file_AceEditorAceeditor', $parameter);
            $cells['edit'] = $this->getButton($url);
        }
    }

    /**
     * Get the file object of the given cell information.
     *
     * @param array $cells
     *
     * @return File
     */
    protected function getFile($cells)
    {
        return $cells['__fileOrFolderObject'];
    }

    /**
     * Get the download button with the given URI.
     *
     * @param string|null $uri
     * @param string $additionalClass
     *
     * @return string
     */
    public function getButton($uri = null, $additionalClass = null)
    {
        $label = 'Edit file with ACE editor';
        $spriteIcon = $this->getWizardIcon();
        if (null === $uri) {
            return '<span class="btn btn-default disabled" title="' . $label . '">' . $spriteIcon . '</span>';
        }
        return '<a href="' . $uri . '" target="_blank" class="btn btn-default' . ($additionalClass ? ' ' . $additionalClass : '') . '" title="' . $label . '">' . $spriteIcon . '</a>';
    }

    /**
     * Get the download icon.
     *
     * @return string
     */
    protected function getWizardIcon()
    {
        /** @var IconFactory $iconFactory */
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        $icon = $iconFactory->getIcon('actions-page-open', Icon::SIZE_SMALL, null);
        return $icon->render();
    }
}

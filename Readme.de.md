# Ace-Editor Erweiterung für TYPO3

Diese Erweiterung integriert den ACE-Editor in das TYPO3-Backend.


**Features:**

*   Komfortable Bearbeitung von Source-Dateien innerhalb der Filelist
*   Eigenes Backendmodul zur besseren Übersichtlichkeit
*   Umfangreiche Editing-Features wie z.B. Syntax-Highlighting

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Ace-Editor Dokumentation](https://www.coding.ms/documentation/typo3-ace-editor "Ace-Editor Dokumentation")
*   [Ace-Editor Bug-Tracker](https://gitlab.com/codingms/typo3-public/ace_editor/-/issues "Ace-Editor Bug-Tracker")
*   [Ace-Editor Repository](https://gitlab.com/codingms/typo3-public/ace_editor "Ace-Editor Repository")
*   [Ace Editor Website](https://ace.c9.io/ "Ace Editor Website")
*   [TYPO3 Ace-Editor Produktdetails](https://www.coding.ms/typo3-extensions/typo3-ace-editor/ "TYPO3 Ace-Editor Produktdetails")
*   [TYPO3 Extension Ace Editor Download](https://extensions.typo3.org/extension/ace_editor/ "TYPO3 Extension Ace Editor Download")

# ACE-Editor Change-Log

## 2023-11-01  Release of version 2.2.0

*	[TASK] Maintenance for TYPO3 11 and remove support for TYPO3 9
*	[TASK] Optimize documentation features
*	[TASK] Optimize documentation metadata
*	[TASK] Optimize configuration and documentation
*	[TASK] Add documentations configuration



## 2021-10-17  Release of version 2.1.0

*	[TASK] Migration for TYPO3 11



## 2021-01-05  Release of version 2.0.2

*	[TASK] Add german translations



## 2020-08-20  Release of version 2.0.1

*	[TASK] Add extra tags in composer.json
*	[TASK] Replace _EXTKEY variable with extension key



## 2020-08-17  Release of version 2.0.0

*	[TASK] Migration for TYPO3 10.4



## 2019-10-12  Release of version 1.1.1

*	[TASK] Modify Gitlab-CI configuration.



## 2019-03-06  Release of version 1.1.0

*	[FEATURE] Add Gitlab-CI configuration.
*	[BUGFIX] Fix some small issues.
